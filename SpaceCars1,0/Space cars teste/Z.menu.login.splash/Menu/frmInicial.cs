﻿using Space_cars_teste.User_Controls.Módulo_de_Acesso.Funcionario;
using Space_cars_teste.User_Controls.Módulo_de_Compra.Compra;
using Space_cars_teste.User_Controls.Módulo_de_Compra.Fornecedor;
using Space_cars_teste.User_Controls.Módulo_de_Compra.Produto;
using Space_cars_teste.User_Controls.Módulo_de_Estoque;
using Space_cars_teste.User_Controls.Módulo_de_RH;
using Space_cars_teste.User_Controls.Módulo_de_Venda.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Space_cars_teste.Forms.Menu
{
    public partial class frmInicial : Form
    {
        public frmInicial()
        {
            InitializeComponent();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void menuStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            
        }

        private void cadrastrarFuncionariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrarFuncionario tela = new frmCadastrarFuncionario();
            tela.Show();
            this.Hide();
        }

        private void consultarFuncionariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultarFuncionario tela = new frmConsultarFuncionario();
            tela.Show();
            this.Hide();
        }

        private void adicionarrItensToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            DialogResult b = MessageBox.Show("Deseja fechar o programa?",
                                            "SpaceCars",
                                            MessageBoxButtons.YesNo,
                                            MessageBoxIcon.Question);

            if(b ==  DialogResult.Yes)
            {
                Close();
            }

        }

        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void frmInicial_Load(object sender, EventArgs e)
        {

        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrarFuncionario tela = new frmCadastrarFuncionario();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultarFuncionario tela = new frmConsultarFuncionario();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmCadastrar_Cliente tela = new frmCadastrar_Cliente();
            tela.Show();
            this.Hide();
        }

        private void cunsultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsulta_de_Cliente tela = new frmConsulta_de_Cliente();
            tela.Show();
            this.Hide();
        }

        private void novaCompraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCompra tela = new frmCompra();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmConsultarCompra tela = new frmConsultarCompra();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmCadastrarProduto tela = new frmCadastrarProduto();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmConsultarProduto tela = new frmConsultarProduto();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            frmCadastrarFornecedor tela = new frmCadastrarFornecedor();
            tela.Show();
            this.Hide();
        }

        private void baterPontoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBaterPonto tela = new frmBaterPonto();
            tela.Show();
            this.Hide();
        }

        private void folhaDePagamntoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmFolhadePagamento tela = new frmFolhadePagamento();
            tela.Show();
            this.Hide();
        }

        private void estoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultar_Estoque tela = new frmConsultar_Estoque();
            tela.Show();
            this.Hide();
        }
    }
}
