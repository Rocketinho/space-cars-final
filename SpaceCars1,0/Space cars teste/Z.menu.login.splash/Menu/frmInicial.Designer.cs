﻿namespace Space_cars_teste.Forms.Menu
{
    partial class frmInicial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInicial));
            this.menuStrip8 = new System.Windows.Forms.MenuStrip();
            this.funcionarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rHToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estoqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fluxoDeCaixaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cunsultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comprarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novaCompraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.produtoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.fornecedorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.venderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novaVendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.produtoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.baterPontoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folhaDePagamntoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuStrip8.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip8
            // 
            this.menuStrip8.BackgroundImage = global::Space_cars_teste.Properties.Resources.Imagem_de_fundo;
            this.menuStrip8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuStrip8.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.funcionarioToolStripMenuItem,
            this.clientesToolStripMenuItem,
            this.compraToolStripMenuItem,
            this.vendaToolStripMenuItem,
            this.rHToolStripMenuItem,
            this.estoqueToolStripMenuItem,
            this.fluxoDeCaixaToolStripMenuItem});
            this.menuStrip8.Location = new System.Drawing.Point(0, 0);
            this.menuStrip8.Name = "menuStrip8";
            this.menuStrip8.Size = new System.Drawing.Size(711, 31);
            this.menuStrip8.TabIndex = 14;
            this.menuStrip8.Text = "menuStrip8";
            // 
            // funcionarioToolStripMenuItem
            // 
            this.funcionarioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem,
            this.consultarToolStripMenuItem});
            this.funcionarioToolStripMenuItem.Font = new System.Drawing.Font("Sitka Display", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.funcionarioToolStripMenuItem.ForeColor = System.Drawing.Color.Silver;
            this.funcionarioToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.funcionarioToolStripMenuItem.Name = "funcionarioToolStripMenuItem";
            this.funcionarioToolStripMenuItem.Size = new System.Drawing.Size(97, 27);
            this.funcionarioToolStripMenuItem.Text = "Funcionario";
            // 
            // clientesToolStripMenuItem
            // 
            this.clientesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem1,
            this.cunsultarToolStripMenuItem});
            this.clientesToolStripMenuItem.Font = new System.Drawing.Font("Sitka Display", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientesToolStripMenuItem.ForeColor = System.Drawing.Color.Silver;
            this.clientesToolStripMenuItem.Name = "clientesToolStripMenuItem";
            this.clientesToolStripMenuItem.Size = new System.Drawing.Size(71, 27);
            this.clientesToolStripMenuItem.Text = "Clientes";
            // 
            // compraToolStripMenuItem
            // 
            this.compraToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.comprarToolStripMenuItem,
            this.produtoToolStripMenuItem,
            this.fornecedorToolStripMenuItem});
            this.compraToolStripMenuItem.Font = new System.Drawing.Font("Sitka Display", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.compraToolStripMenuItem.ForeColor = System.Drawing.Color.Silver;
            this.compraToolStripMenuItem.Name = "compraToolStripMenuItem";
            this.compraToolStripMenuItem.Size = new System.Drawing.Size(71, 27);
            this.compraToolStripMenuItem.Text = "Compra";
            // 
            // vendaToolStripMenuItem
            // 
            this.vendaToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.vendaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.venderToolStripMenuItem,
            this.produtoToolStripMenuItem1});
            this.vendaToolStripMenuItem.Font = new System.Drawing.Font("Sitka Display", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vendaToolStripMenuItem.ForeColor = System.Drawing.Color.Silver;
            this.vendaToolStripMenuItem.Name = "vendaToolStripMenuItem";
            this.vendaToolStripMenuItem.Size = new System.Drawing.Size(60, 27);
            this.vendaToolStripMenuItem.Text = "Venda";
            // 
            // rHToolStripMenuItem
            // 
            this.rHToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.baterPontoToolStripMenuItem,
            this.folhaDePagamntoToolStripMenuItem});
            this.rHToolStripMenuItem.Font = new System.Drawing.Font("Sitka Display", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rHToolStripMenuItem.ForeColor = System.Drawing.Color.Silver;
            this.rHToolStripMenuItem.Name = "rHToolStripMenuItem";
            this.rHToolStripMenuItem.Size = new System.Drawing.Size(42, 27);
            this.rHToolStripMenuItem.Text = "RH";
            // 
            // estoqueToolStripMenuItem
            // 
            this.estoqueToolStripMenuItem.Font = new System.Drawing.Font("Sitka Display", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estoqueToolStripMenuItem.ForeColor = System.Drawing.Color.Silver;
            this.estoqueToolStripMenuItem.Name = "estoqueToolStripMenuItem";
            this.estoqueToolStripMenuItem.Size = new System.Drawing.Size(73, 27);
            this.estoqueToolStripMenuItem.Text = "Estoque";
            this.estoqueToolStripMenuItem.Click += new System.EventHandler(this.estoqueToolStripMenuItem_Click);
            // 
            // fluxoDeCaixaToolStripMenuItem
            // 
            this.fluxoDeCaixaToolStripMenuItem.Font = new System.Drawing.Font("Sitka Display", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fluxoDeCaixaToolStripMenuItem.ForeColor = System.Drawing.Color.Silver;
            this.fluxoDeCaixaToolStripMenuItem.Name = "fluxoDeCaixaToolStripMenuItem";
            this.fluxoDeCaixaToolStripMenuItem.Size = new System.Drawing.Size(111, 27);
            this.fluxoDeCaixaToolStripMenuItem.Text = "Fluxo de caixa";
            // 
            // cadastrarToolStripMenuItem
            // 
            this.cadastrarToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cadastrarToolStripMenuItem.BackgroundImage")));
            this.cadastrarToolStripMenuItem.Font = new System.Drawing.Font("Sitka Display", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cadastrarToolStripMenuItem.ForeColor = System.Drawing.Color.Silver;
            this.cadastrarToolStripMenuItem.Name = "cadastrarToolStripMenuItem";
            this.cadastrarToolStripMenuItem.Size = new System.Drawing.Size(180, 24);
            this.cadastrarToolStripMenuItem.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem.Click += new System.EventHandler(this.cadastrarToolStripMenuItem_Click);
            // 
            // consultarToolStripMenuItem
            // 
            this.consultarToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem.BackgroundImage")));
            this.consultarToolStripMenuItem.Font = new System.Drawing.Font("Sitka Display", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consultarToolStripMenuItem.ForeColor = System.Drawing.Color.Silver;
            this.consultarToolStripMenuItem.Name = "consultarToolStripMenuItem";
            this.consultarToolStripMenuItem.Size = new System.Drawing.Size(180, 24);
            this.consultarToolStripMenuItem.Text = "Consultar";
            this.consultarToolStripMenuItem.Click += new System.EventHandler(this.consultarToolStripMenuItem_Click);
            // 
            // cadastrarToolStripMenuItem1
            // 
            this.cadastrarToolStripMenuItem1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cadastrarToolStripMenuItem1.BackgroundImage")));
            this.cadastrarToolStripMenuItem1.ForeColor = System.Drawing.Color.Silver;
            this.cadastrarToolStripMenuItem1.Name = "cadastrarToolStripMenuItem1";
            this.cadastrarToolStripMenuItem1.Size = new System.Drawing.Size(180, 26);
            this.cadastrarToolStripMenuItem1.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem1.Click += new System.EventHandler(this.cadastrarToolStripMenuItem1_Click);
            // 
            // cunsultarToolStripMenuItem
            // 
            this.cunsultarToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cunsultarToolStripMenuItem.BackgroundImage")));
            this.cunsultarToolStripMenuItem.ForeColor = System.Drawing.Color.Silver;
            this.cunsultarToolStripMenuItem.Name = "cunsultarToolStripMenuItem";
            this.cunsultarToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.cunsultarToolStripMenuItem.Text = "Cunsultar";
            this.cunsultarToolStripMenuItem.Click += new System.EventHandler(this.cunsultarToolStripMenuItem_Click);
            // 
            // comprarToolStripMenuItem
            // 
            this.comprarToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("comprarToolStripMenuItem.BackgroundImage")));
            this.comprarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novaCompraToolStripMenuItem,
            this.consultarToolStripMenuItem1});
            this.comprarToolStripMenuItem.ForeColor = System.Drawing.Color.Silver;
            this.comprarToolStripMenuItem.Name = "comprarToolStripMenuItem";
            this.comprarToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.comprarToolStripMenuItem.Text = "Comprar";
            // 
            // novaCompraToolStripMenuItem
            // 
            this.novaCompraToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("novaCompraToolStripMenuItem.BackgroundImage")));
            this.novaCompraToolStripMenuItem.ForeColor = System.Drawing.Color.Silver;
            this.novaCompraToolStripMenuItem.Name = "novaCompraToolStripMenuItem";
            this.novaCompraToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.novaCompraToolStripMenuItem.Text = "Fazer Compra";
            this.novaCompraToolStripMenuItem.Click += new System.EventHandler(this.novaCompraToolStripMenuItem_Click);
            // 
            // consultarToolStripMenuItem1
            // 
            this.consultarToolStripMenuItem1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem1.BackgroundImage")));
            this.consultarToolStripMenuItem1.ForeColor = System.Drawing.Color.Silver;
            this.consultarToolStripMenuItem1.Name = "consultarToolStripMenuItem1";
            this.consultarToolStripMenuItem1.Size = new System.Drawing.Size(180, 26);
            this.consultarToolStripMenuItem1.Text = "Consultar ";
            this.consultarToolStripMenuItem1.Click += new System.EventHandler(this.consultarToolStripMenuItem1_Click);
            // 
            // produtoToolStripMenuItem
            // 
            this.produtoToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("produtoToolStripMenuItem.BackgroundImage")));
            this.produtoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem2,
            this.consultarToolStripMenuItem2});
            this.produtoToolStripMenuItem.ForeColor = System.Drawing.Color.Silver;
            this.produtoToolStripMenuItem.Name = "produtoToolStripMenuItem";
            this.produtoToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.produtoToolStripMenuItem.Text = "Produto";
            // 
            // cadastrarToolStripMenuItem2
            // 
            this.cadastrarToolStripMenuItem2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cadastrarToolStripMenuItem2.BackgroundImage")));
            this.cadastrarToolStripMenuItem2.ForeColor = System.Drawing.Color.Silver;
            this.cadastrarToolStripMenuItem2.Name = "cadastrarToolStripMenuItem2";
            this.cadastrarToolStripMenuItem2.Size = new System.Drawing.Size(180, 26);
            this.cadastrarToolStripMenuItem2.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem2.Click += new System.EventHandler(this.cadastrarToolStripMenuItem2_Click);
            // 
            // consultarToolStripMenuItem2
            // 
            this.consultarToolStripMenuItem2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem2.BackgroundImage")));
            this.consultarToolStripMenuItem2.ForeColor = System.Drawing.Color.Silver;
            this.consultarToolStripMenuItem2.Name = "consultarToolStripMenuItem2";
            this.consultarToolStripMenuItem2.Size = new System.Drawing.Size(180, 26);
            this.consultarToolStripMenuItem2.Text = "Consultar";
            this.consultarToolStripMenuItem2.Click += new System.EventHandler(this.consultarToolStripMenuItem2_Click);
            // 
            // fornecedorToolStripMenuItem
            // 
            this.fornecedorToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("fornecedorToolStripMenuItem.BackgroundImage")));
            this.fornecedorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem3,
            this.consultarToolStripMenuItem3});
            this.fornecedorToolStripMenuItem.ForeColor = System.Drawing.Color.Silver;
            this.fornecedorToolStripMenuItem.Name = "fornecedorToolStripMenuItem";
            this.fornecedorToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.fornecedorToolStripMenuItem.Text = "Fornecedor";
            // 
            // cadastrarToolStripMenuItem3
            // 
            this.cadastrarToolStripMenuItem3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cadastrarToolStripMenuItem3.BackgroundImage")));
            this.cadastrarToolStripMenuItem3.ForeColor = System.Drawing.Color.Silver;
            this.cadastrarToolStripMenuItem3.Name = "cadastrarToolStripMenuItem3";
            this.cadastrarToolStripMenuItem3.Size = new System.Drawing.Size(180, 26);
            this.cadastrarToolStripMenuItem3.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem3.Click += new System.EventHandler(this.cadastrarToolStripMenuItem3_Click);
            // 
            // consultarToolStripMenuItem3
            // 
            this.consultarToolStripMenuItem3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem3.BackgroundImage")));
            this.consultarToolStripMenuItem3.ForeColor = System.Drawing.Color.Silver;
            this.consultarToolStripMenuItem3.Name = "consultarToolStripMenuItem3";
            this.consultarToolStripMenuItem3.Size = new System.Drawing.Size(180, 26);
            this.consultarToolStripMenuItem3.Text = "Consultar";
            // 
            // venderToolStripMenuItem
            // 
            this.venderToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("venderToolStripMenuItem.BackgroundImage")));
            this.venderToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novaVendaToolStripMenuItem,
            this.consultarToolStripMenuItem4});
            this.venderToolStripMenuItem.ForeColor = System.Drawing.Color.Silver;
            this.venderToolStripMenuItem.Name = "venderToolStripMenuItem";
            this.venderToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.venderToolStripMenuItem.Text = "Vender";
            // 
            // novaVendaToolStripMenuItem
            // 
            this.novaVendaToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("novaVendaToolStripMenuItem.BackgroundImage")));
            this.novaVendaToolStripMenuItem.ForeColor = System.Drawing.Color.Silver;
            this.novaVendaToolStripMenuItem.Name = "novaVendaToolStripMenuItem";
            this.novaVendaToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.novaVendaToolStripMenuItem.Text = "Nova Venda";
            // 
            // consultarToolStripMenuItem4
            // 
            this.consultarToolStripMenuItem4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem4.BackgroundImage")));
            this.consultarToolStripMenuItem4.ForeColor = System.Drawing.Color.Silver;
            this.consultarToolStripMenuItem4.Name = "consultarToolStripMenuItem4";
            this.consultarToolStripMenuItem4.Size = new System.Drawing.Size(180, 26);
            this.consultarToolStripMenuItem4.Text = "Consultar";
            // 
            // produtoToolStripMenuItem1
            // 
            this.produtoToolStripMenuItem1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("produtoToolStripMenuItem1.BackgroundImage")));
            this.produtoToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem4,
            this.consultarToolStripMenuItem5});
            this.produtoToolStripMenuItem1.ForeColor = System.Drawing.Color.Silver;
            this.produtoToolStripMenuItem1.Name = "produtoToolStripMenuItem1";
            this.produtoToolStripMenuItem1.Size = new System.Drawing.Size(180, 26);
            this.produtoToolStripMenuItem1.Text = "Produto";
            // 
            // cadastrarToolStripMenuItem4
            // 
            this.cadastrarToolStripMenuItem4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cadastrarToolStripMenuItem4.BackgroundImage")));
            this.cadastrarToolStripMenuItem4.ForeColor = System.Drawing.Color.Silver;
            this.cadastrarToolStripMenuItem4.Name = "cadastrarToolStripMenuItem4";
            this.cadastrarToolStripMenuItem4.Size = new System.Drawing.Size(180, 26);
            this.cadastrarToolStripMenuItem4.Text = "Cadastrar";
            // 
            // consultarToolStripMenuItem5
            // 
            this.consultarToolStripMenuItem5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem5.BackgroundImage")));
            this.consultarToolStripMenuItem5.ForeColor = System.Drawing.Color.Silver;
            this.consultarToolStripMenuItem5.Name = "consultarToolStripMenuItem5";
            this.consultarToolStripMenuItem5.Size = new System.Drawing.Size(180, 26);
            this.consultarToolStripMenuItem5.Text = "Consultar";
            // 
            // baterPontoToolStripMenuItem
            // 
            this.baterPontoToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("baterPontoToolStripMenuItem.BackgroundImage")));
            this.baterPontoToolStripMenuItem.ForeColor = System.Drawing.Color.Silver;
            this.baterPontoToolStripMenuItem.Name = "baterPontoToolStripMenuItem";
            this.baterPontoToolStripMenuItem.Size = new System.Drawing.Size(201, 26);
            this.baterPontoToolStripMenuItem.Text = "Bater Ponto";
            this.baterPontoToolStripMenuItem.Click += new System.EventHandler(this.baterPontoToolStripMenuItem_Click);
            // 
            // folhaDePagamntoToolStripMenuItem
            // 
            this.folhaDePagamntoToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("folhaDePagamntoToolStripMenuItem.BackgroundImage")));
            this.folhaDePagamntoToolStripMenuItem.ForeColor = System.Drawing.Color.Silver;
            this.folhaDePagamntoToolStripMenuItem.Name = "folhaDePagamntoToolStripMenuItem";
            this.folhaDePagamntoToolStripMenuItem.Size = new System.Drawing.Size(201, 26);
            this.folhaDePagamntoToolStripMenuItem.Text = "Folha de Pagamnto";
            this.folhaDePagamntoToolStripMenuItem.Click += new System.EventHandler(this.folhaDePagamntoToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.menuStrip8);
            this.panel1.Location = new System.Drawing.Point(-6, -2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(715, 35);
            this.panel1.TabIndex = 15;
            // 
            // frmInicial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(65)))), ((int)(((byte)(71)))));
            this.BackgroundImage = global::Space_cars_teste.Properties.Resources.Tela_de_Login1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(700, 410);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.Color.Silver;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmInicial";
            this.Text = "frmInicial";
            this.Load += new System.EventHandler(this.frmInicial_Load);
            this.menuStrip8.ResumeLayout(false);
            this.menuStrip8.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip8;
        private System.Windows.Forms.ToolStripMenuItem funcionarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rHToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estoqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fluxoDeCaixaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cunsultarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comprarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novaCompraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem produtoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem fornecedorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem venderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novaVendaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem produtoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem baterPontoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem folhaDePagamntoToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
    }
}