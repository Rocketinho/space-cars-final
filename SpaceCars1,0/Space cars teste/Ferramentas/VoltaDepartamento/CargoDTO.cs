﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.VoltarDepartamento
{
    public class CargoDTO
    {
        public int ID_Cargo { get; set; }

        public string Cargo { get; set; }
    }
}
