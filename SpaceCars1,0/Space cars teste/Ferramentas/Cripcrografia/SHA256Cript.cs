﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.Criptografia
{
    public class SHA256Cript
    {
        //Esse método apenas CRIPTOGRAFA a senha. 
        //Conhecido como método Hash de criptografia é tido como o mais seguro.
        public string Criptografar(string mensagem)
        {
            //Cria uma instância do HASH SHA256
            System.Security.Cryptography.SHA256 sha256 = System.Security.Cryptography.SHA256.Create();

            //Transforma a mensagem passada por parâmetro em binário
            byte[] mensagemBytes = Encoding.UTF8.GetBytes(mensagem);

            //Passo valores de acordo com o HASH SHA256
            byte[] criptografiaBytes = sha256.ComputeHash(mensagemBytes);

            //Para o valor binário para uma string.
            string criptografia = Convert.ToBase64String(criptografiaBytes);

            return criptografia;
        }
    }
}
