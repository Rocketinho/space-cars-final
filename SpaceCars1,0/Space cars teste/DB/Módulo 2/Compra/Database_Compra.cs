﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras.Database
{
    public class Database_Compra
    {
        public int Salvar (DTO_Compra dto)
        {
            string script =
            @"INSERT INTO tb_compra_fornecedor
            (
                id_fornecedor,
                id_produto,
                id_funcionario,
                dt_compra,
                ds_lte,
                dt_validade
            )
            VALUES
            (
                @id_fornecedor,
                @id_produto,
                @id_funcionario,
                @dt_compra,
                @ds_lte,
                @dt_validade
            )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_fornecedor", dto.ID_Fornecedor));
            parm.Add(new MySqlParameter("id_produto", dto.Id_produto));
            parm.Add(new MySqlParameter("id_funcionario", dto.Id_funcionario));
            parm.Add(new MySqlParameter("dt_compra", dto.Data_Compra));
            parm.Add(new MySqlParameter("ds_lte", dto.Lote));
            parm.Add(new MySqlParameter("dt_validade", dto.Validade));




            Space_cars_teste.DB.Base.Database db = new Space_cars_teste.DB.Base.Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        public void Alterar(DTO_Compra dto)
        {
           
        }

        
    }
}
