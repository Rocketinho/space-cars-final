﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras
{
    public class DTO_Produto
    {
        public int ID { get; set; }
        public int ID_Fornecedor { get; set; }
        public string Produto { get; set; }
        public Decimal Preco { get; set; }
        public string Marca { get; set; }
        public string Imagem { get; set; }
    }
}
