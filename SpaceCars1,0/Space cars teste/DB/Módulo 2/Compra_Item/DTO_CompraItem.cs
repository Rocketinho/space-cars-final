﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.PuroTempero.MODELO.Módulo_Compras
{
    public class DTO_CompraItem
    {
        public int ID { get; set; }
        public int ID_Compra { get; set; }
        public int ID_Produto { get; set; }
    }
}
