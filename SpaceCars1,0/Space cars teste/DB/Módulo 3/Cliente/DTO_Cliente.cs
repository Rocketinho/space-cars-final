﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Cliente
{
    public class DTO_Cliente
    {
        public int ID_Cliente { get; set; }
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public DateTime Nascimento { get; set; }
        public string CPF { get; set; }
        public string RG { get; set; }
        public bool Sexo { get; set; }
        public string PratoFavorito { get; set; }
        public string Frequencia { get; set; }
        public string Imagem { get; set; }
        public string MesaFavorita { get; set; }
        public string Email { get; set; }
        public string Endereco { get; set; }
        public string CEP { get; set; }
        public string Complemento { get; set; }
        public int Casa { get; set; }
        public string UF { get; set; }
        public string Cidade { get; set; }
    }
}
