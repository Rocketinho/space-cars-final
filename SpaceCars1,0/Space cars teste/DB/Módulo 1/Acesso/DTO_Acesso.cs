﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Space_cars_teste.DB.Modelo_de_acesso.DTO
{
    public class DTO_Acesso
    {
        public int ID { get; set; }

        public int ID_Funcionario { get; set; }

        public int ID_Cargo{ get; set; }

        public bool Salvar { get; set; }

        public bool Remover { get; set; }

        public bool Alterar { get; set; }

        public bool Consultar { get; set; }
    }
}
