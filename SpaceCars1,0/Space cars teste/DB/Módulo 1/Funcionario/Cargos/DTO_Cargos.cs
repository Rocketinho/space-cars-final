﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Space_cars_teste.DB.Modelo_de_acesso.Funcionario
{
     class DTO_Cargos
    {
        public int ID { get; set; }

        public string  Nome { get; set; }

        public decimal  Salario { get; set; }

        public decimal HorasExtras { get; set; }
    }
}
