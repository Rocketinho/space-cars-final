﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Space_cars_teste.DB.Modelo_de_acesso.Funcionario.Cargos
{
    class Database_Cargos
    {
        public int Salvar(DTO_Cargos dto)
        {
            string script = @"INSERT INTO tb_cargo(
                            nm_cargo,
                            ds_salario,
                            ds_horaextra

                VALUES      @nm_cargo,
                            @ds_salario,
                            @ds_horaextra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cargo", dto.Nome));
            parms.Add(new MySqlParameter("ds_salario", dto.Salario));
            parms.Add(new MySqlParameter("ds_horaextra", dto.HorasExtras));

            DB.Base.Database db = new Base.Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }



    }
}
