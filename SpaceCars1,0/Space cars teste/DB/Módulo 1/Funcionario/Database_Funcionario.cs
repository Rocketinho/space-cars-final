﻿using MySql.Data.MySqlClient;
using Space_cars_teste.DB.Modelo_de_acesso.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Space_cars_teste.DB.Modelo_de_acesso.Database
{
    public class Database_Funcionario
    {
        public int Salvar(DTO_Funcionario dto)
        {
            string script =
                @"INSERT INTO tb_funcionario(nm_funcionario, ds_RG, ds_CPF, dt_nascimento, tp_sexo, vl_vale_refeicao, vl_salario, img_foto, ds_usuario, ds_senha, nm_uf, nm_cidade, nm_endereco, nr_residencia, ds_CEP, ds_complemento, ds_email, ds_telefone, ds_celular)
                  VALUES (@nm_funcionario, @ds_RG, @ds_CPF, @dt_nascimento, @tp_sexo, @vl_vale_refeicao @vl_salario, @img_foto, @ds_usuario, @ds_senha,@nm_uf, @nm_cidade, @nm_endereco, @nr_residencia, @ds_CEP, @ds_complemento, @ds_email, @ds_telefone, @ds_celular) ";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome));
            parms.Add(new MySqlParameter("ds_RG", dto.RG));
            parms.Add(new MySqlParameter("ds_CPF", dto.CPF));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("tp_sexo", dto.Sexo));
            parms.Add(new MySqlParameter("vl_vale_refeicao", dto.Refeicao));
            parms.Add(new MySqlParameter("vl_salario", dto.Salario));
            parms.Add(new MySqlParameter("img_foto", dto.Foto));
            parms.Add(new MySqlParameter("ds_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("nm_uf", dto.UF));
            parms.Add(new MySqlParameter("nm_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("nm_endereco", dto.Endereço));
            parms.Add(new MySqlParameter("nr_residencia", dto.Numero));
            parms.Add(new MySqlParameter("ds_CEP", dto.CEP));
            parms.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_celular", dto.Celular));

            DB.Base.Database db = new DB.Base.Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(DTO_Funcionario dto)
        {
            string script =
                @"Update tb_funcionario SET nm_funcionario = @nm_funcionario,
                                            ds_RG = @ds_RG,
                                            ds_CPF = @ds_CPF,
                                            dt_nascimento = @dt_nascimento,
                                            tp_sexo = @tp_sexo,
                                            vl_vale_refeicao = @vl_vale_refeicao,
                                            vl_salario = @vl_salario,
                                            img_foto = @img_foto,
                                            ds_usuario = @ds_usuario,
                                            ds_senha = @ds_senha,
                                            nm_uf = @nm_uf,
                                            nm_cidade = @nm_cidade,
                                            nm_endereco = @nm_endereco,
                                            nr_residencia = @nr_residencia,
                                            ds_CEP = @ds_CEP,
                                            ds_complemento = @ds_complemento,
                                            ds_email = @ds_email,
                                            ds_telefone = @ds_telefone,
                                            ds_celular = @ds_celular
                   WHERE id_funcionario = @id_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.Id));
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome));
            parms.Add(new MySqlParameter("ds_RG", dto.RG));
            parms.Add(new MySqlParameter("ds_CPF", dto.CPF));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("tp_sexo", dto.Sexo));
            parms.Add(new MySqlParameter("vl_vale_refeicao", dto.Refeicao));
            parms.Add(new MySqlParameter("vl_salario", dto.Salario));
            parms.Add(new MySqlParameter("img_foto", dto.Foto));
            parms.Add(new MySqlParameter("ds_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("nm_uf", dto.UF));
            parms.Add(new MySqlParameter("nm_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("nm_endereco", dto.Endereço));
            parms.Add(new MySqlParameter("nr_residencia", dto.Numero));
            parms.Add(new MySqlParameter("ds_CEP", dto.CEP));
            parms.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_celular", dto.Celular));

            DB.Base.Database db = new DB.Base.Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(DTO_Funcionario dto)
        {
            string script = @"DELETE FROM tb_funcionario WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.Id));

            DB.Base.Database db = new DB.Base.Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<DTO_Funcionario> Consultar(DTO_Funcionario dto)
        {
            string script = @"SELECT * FROM tb_funcionario
            WHERE nm_funcionario like @nm_funcionario AND 
                  ds_RG like @ds_RG AND
                  ds_CPF like @ds_CPF";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", "%" + dto.Nome + "%"));
            parms.Add(new MySqlParameter("ds_RG", "%" + dto.RG + "%"));
            parms.Add(new MySqlParameter("ds_CPF", "%" + dto.CPF + "%"));
            DB.Base.Database db = new DB.Base.Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Funcionario> lista = new List<DTO_Funcionario>();
            while (reader.Read())
            {
                DTO_Funcionario dt = new DTO_Funcionario();
                dt.Id = reader.GetInt32("id_funcionario");
                dt.Nome = reader.GetString("nm_funcionario");
                dt.CPF = reader.GetString("ds_CPF");
                dt.RG = reader.GetString("ds_RG");
                dt.Telefone = reader.GetString("ds_telefone");
                dt.Nascimento = reader.GetDateTime("dt_nascimento");
                dt.Endereço = reader.GetString("nm_endereco");
                dt.UF = reader.GetString("nm_uf");
                lista.Add(dt);

            }

            reader.Close();
            return lista;
        }

        public DTO_Funcionario Logar(DTO_Funcionario dto)
        {
            string script = @"SELECT * FROM tb_funcionario
                                      WHERE ds_usuario =@ds_usuario
                                      AND   ds_senha   =@ds_senha";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ds_usuario", dto.Usuario));
            parm.Add(new MySqlParameter("ds_senha", dto.Senha));

            DB.Base.Database db = new DB.Base.Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            DTO_Funcionario fora = null;

            if (reader.Read())
            {
                fora = new DTO_Funcionario();
                fora.Id = reader.GetInt32("id_funcionario");
                fora.Nome = reader.GetString("nm_funcionario");
                fora.RG = reader.GetString("ds_rg");
                fora.CPF = reader.GetString("ds_cpf");
                fora.Nascimento = reader.GetDateTime("dt_nascimento");
                fora.Sexo = reader.GetString("tp_sexo");
                fora.Refeicao = reader.GetDecimal("vl_vale_refeicao");
                fora.Salario = reader.GetDecimal("vl_salario");
                fora.Foto = reader.GetString("img_foto");
                fora.Complemento = reader.GetString("ds_complemento");
                fora.Usuario = reader.GetString("ds_usuario");
                fora.Senha = reader.GetString("ds_senha");
                fora.UF = reader.GetString("nm_uf");
                fora.Cidade = reader.GetString("nm_cidade");
                fora.Endereço = reader.GetString("nm_endereco");
                fora.Numero = reader.GetInt32("nr_residencia");
                fora.CEP = reader.GetString("ds_cep");
                fora.Complemento = reader.GetString("ds_complemento");
                fora.Email = reader.GetString("ds_email");
                fora.Telefone = reader.GetString("ds_telefone");
                fora.Celular = reader.GetString("ds_celular");
            }

            reader.Close();
            return fora;
        }

        //Servindo aqui como meio de descriptografar as senhas.
        public bool VerificarSenha(string senha)
        {
            string script = @"SELECT * FROM tb_funcionario
                                       WHERE ds_senha = @ds_senha";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ds_senha", senha));

            DB.Base.Database db = new DB.Base.Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<DTO_Funcionario> ListarCargo(DTO_Funcionario dto)

        {
            string script = @"SELECT * FROM tb_cargo";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", "%" + dto.Nome + "%"));
            parms.Add(new MySqlParameter("ds_RG", "%" + dto.RG + "%"));
            parms.Add(new MySqlParameter("ds_CPF", "%" + dto.CPF + "%"));

            DB.Base.Database Db = new DB.Base.Database();
            MySqlDataReader reader = Db.ExecuteSelectScript(script, parms);
            List<DTO_Funcionario> lista = new List<DTO_Funcionario>();

            while (reader.Read())
            {
                DTO_Funcionario dt = new DTO_Funcionario();
                dt.Id = reader.GetInt32("id_funcionario");
                dt.Nome = reader.GetString("nm_funcionario");
                dt.CPF = reader.GetString("ds_CPF");
                dt.RG = reader.GetString("ds_RG");
                dt.Telefone = reader.GetString("ds_telefone");
                dt.Nascimento = reader.GetDateTime("dt_nascimento");
                dt.Endereço = reader.GetString("nm_endereco");
                dt.UF = reader.GetString("nm_uf");
                lista.Add(dt);

            }

            reader.Close();
            return lista;
        }
    }
    
}
