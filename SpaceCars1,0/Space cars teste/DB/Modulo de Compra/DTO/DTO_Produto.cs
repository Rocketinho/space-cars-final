﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Space_cars_teste.nsfspace_Car_DTO_s.Módulo_de_Compra
{
    public class DTO_Produto
    {
       
         public int ID { get; set; }
         public int ID_Fornecedor { get; set; }
         public string Produto { get; set; }
         public Decimal Preco { get; set; }
         public string Marca { get; set; }
         public string Imagem { get; set; }
        
    }
}
