﻿using Nsf.PuroTempero.MODELO.Módulo_Estoque;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Space_cars_teste.User_Controls.Módulo_de_Estoque
{
    public partial class frmConsultar_Estoque : Form
    {
        public frmConsultar_Estoque()
        {
            InitializeComponent();
        }

        void Buscar()
        {
            VIEW_EstoqueDTO dto = new VIEW_EstoqueDTO();
            dto.Produto = txtProduto.Text;

            View_Estoque view = new View_Estoque();
            List<DTO_EstoqueView> lista = view.Consultar(dto);
            dgvProduto.DataSource = lista;

        }
    }
}
