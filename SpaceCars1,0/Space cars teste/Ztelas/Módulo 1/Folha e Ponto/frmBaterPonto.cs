﻿using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_RH;
using Space_cars_teste.DB.Modelo_de_acesso.Business;
using Space_cars_teste.DB.Modelo_de_acesso.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Space_cars_teste.User_Controls.Módulo_de_RH
{
    public partial class frmBaterPonto : Form
    {
        public frmBaterPonto()
        {
            InitializeComponent();
        }

        private void dateTimePicker3_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void frmBaterPonto_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DTO_Funcionario dto = new DTO_Funcionario();
            dto.Nome = txtNome.Text;
            
            Business_Funcionario business = new Business_Funcionario();
            List<DTO_Funcionario> lista = new List<DTO_Funcionario>();
            lista = business.Consultar(dto);
            dgvFuncionario.AutoGenerateColumns = false;
            dgvFuncionario.DataSource = lista;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DTO_Funcionario dgv = dgvFuncionario.CurrentRow.DataBoundItem as DTO_Funcionario;

            DTO_RH dto = new DTO_RH();
            dto.Data = dtpDia.Value;
            dto.hr_entrada = dtpEntrada.Text;
            dto.hr_almoco_saida = dtpIdaAlmoço.Text;
            dto.hr_almoco_retorno = dtpVoltaAlmoço.Text;
            dto.hr_saida = dtpSaida.Text;

            Business_RH business = new Business_RH();
            business.Salvar(dto);

            dto.id_funcionario = dgvFuncionario.Da_Uma_Olhada_Aqui;
            
        }
    }
}
