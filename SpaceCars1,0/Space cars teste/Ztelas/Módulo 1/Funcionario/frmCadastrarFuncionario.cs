﻿using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.VoltarDepartamento;
using Space_cars_teste.DB.Modelo_de_acesso.Business;
using Space_cars_teste.DB.Modelo_de_acesso.DTO;
using Space_cars_teste.Forms.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Space_cars_teste.User_Controls.Módulo_de_Acesso.Funcionario
{
    public partial class frmCadastrarFuncionario : Form
    {
        public frmCadastrarFuncionario()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
               


                // Salva Permissões na Tabela Nivel de Acesso

                //Checka Permissões de Vendas
            }

        private void chkRemoverFuncionario_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkAlterarFuncionario_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkConsultarEstoque_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkSalvarEstoque_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkRemoverEstoque_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkAlterarEstoque_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void gpbEstoque_Enter(object sender, EventArgs e)
        {

        }

        private void chkConsultarCompras_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkConsultarVendas_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkSalvarVendas_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkRemoverVendas_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkAlterarVendas_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkConsultarRH_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkAlterarRH_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkSalvarRH_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkRemoverRH_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkSalvarCompras_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkRemoverCompras_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkAlterarCompras_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void lblNome_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            frmInicial tela = new frmInicial();
            tela.Show();
            this.Hide();
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            // Salvar Funcionário


            DTO_Funcionario dto = new DTO_Funcionario();
            dto.Nome = txtNome.Text;

            // if para n salvar RG só com a Mask (Não mexer para n afetar outra parte)
            if (txtRG.Text == "  .   .   -")
            {
                txtRG.Text = null;
            }
            else
            {
                dto.RG = txtRG.Text;
            }
            // if para n salvar CPF só com a Mask (Não mexer para n afetar outra parte)


            if (txtCPF.Text == "  .   .   /    -")
            {
                txtCPF.Text = null;
            }
            else
            {
                dto.CPF = txtCPF.Text;
            }

            dto.Nascimento = dtpNascimento.Value;

            if (chkF.Checked == true)
            {
                dto.Sexo = chkF.Text.ToString();
            }
            if (chkM.Checked == true)
            {
                dto.Sexo = chkM.Text.ToString();
            }
            
            dto.Foto = ImagemPlugin.ConverterParaString(imgFuncionario.Image);
            dto.Usuario = txtUsuario.Text;
            dto.Senha = txtSenha.Text;
            dto.UF = cboUF.SelectedItem.ToString();
            dto.Cidade = txtCidade.Text;
            dto.Endereço = txtEndereco.Text;
            dto.Numero = Convert.ToInt32(nudNumero.Value);
            dto.CEP = txtCEP.Text;
            dto.Complemento = txtComplemento.Text;
            dto.Telefone = txtTelefone.Text;
            dto.Celular = txtCelular.Text;

            Business_Funcionario business = new Business_Funcionario();
            int id_funcionario = business.Salvar(dto);

            MessageBox.Show("Funcionario salvo",
                            "SpaceCars",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);

        }

        private void gpbDadosPessoais_Enter(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }
    }
}
